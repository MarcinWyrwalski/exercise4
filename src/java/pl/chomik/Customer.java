package pl.chomik;

public class Customer {

    private String name, password, email, sex;
    private int phoneNR;

    public Customer(String name, String password, String email, int phoneNR, String sex) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.phoneNR = phoneNR;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public int getPhoneNR() {
        return phoneNR;
    }

    public String getSex() {
        return sex;
    }
}
