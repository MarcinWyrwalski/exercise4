package pl.chomik;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {


    //
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        Customer customer;

        // if I understaand - everytime I run doPost method - i will have new instance of Customer. - is my thinking correct?

        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String email = request.getParameter("email");
        int phoneNR = Integer.valueOf(request.getParameter("phoneNr"));
        String radio = request.getParameter("Radio");

        customer = new Customer(name, password, email, phoneNR, radio);
        session.setAttribute("Customer", customer);

        // so I have session object - are my requester are ok then?
        // do I still need to set request and response?
        // or I just missing part of a puzzle here.
        if (name.equals("") || password.equals("")) {
            request.getRequestDispatcher("/registerError.jsp").forward(request, response);
        // also - i think in else condition - i shall put include  - instead forward?
            // so after geting response from logic system - i can send it to proper viev?

        } else {
            request.getRequestDispatcher("/registerSuccess.jsp").forward(request, response);
        }
    }
}
