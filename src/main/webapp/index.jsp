<html>
<body>
<link rel="stylesheet" href="/webjars/bootstrap/4.2.1/css/bootstrap.min.css">

<div class="container">

    <form action="LoginServlet" method="post">
        <form>
            <div class="form-group">
                <label for="nameInput">User name</label>

                <input type="text" name="name" class="form-control"  id="nameInput" aria-describedby="emailHelp"
                >
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" >
            </div>
            <div class="form-group">
                <label for="emailInput">Email address</label>
                <input type="email" name="email" class="form-control" id="emailInput" aria-describedby="emailHelp"
                       placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="phoneNr">Phone nr</label>
                <input type="text" name="phoneNr" class="form-control" value="Phone Number" id="phoneNr" aria-describedby="emailHelp"
                       placeholder="Enter email">
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="Radio" id="MaleRadio" value="Male" checked>
                <label class="form-check-label" for="MaleRadio">
                    Male
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="Radio" id="FemaleRadio" value="Female">
                <label class="form-check-label" for="FemaleRadio">
                    Female
                </label>

            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </form>
</div>
</body>
</html>
