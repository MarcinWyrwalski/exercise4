<%@ page import="pl.chomik.Customer" %><%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 11.02.2019
  Time: 18:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>registerSuccess</title>
</head>
<body>
<link rel="stylesheet" href="/webjars/bootstrap/4.2.1/css/bootstrap.min.css">

<%

    Customer customer = (Customer) session.getAttribute("Customer");
    String name = customer.getName();
    String email = customer.getEmail();
    int phoneNr = Integer.valueOf(customer.getPhoneNR());
    String sex = customer.getSex();

%>
<div class="container">
<table class="table table-striped">
    <thead>

    </thead>
    <tbody>
    <tr>

        <td>Name:</td>
        <td>  <%=name%></td>

    </tr>
    <tr>

        <td>Email</td>
        <td>   <%=email%></td>

    </tr>
    <tr>

        <td>Phone number</td>
        <td>   <%=phoneNr%></td>

    </tr>
    <tr>

        <td>User gender</td>
        <td>   <%=sex%></td>

    </tr>
    </tbody>
</table>

</div>

</body>
</html>
